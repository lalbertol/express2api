-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-02-2019 a las 18:35:00
-- Versión del servidor: 5.7.24-0ubuntu0.16.04.1
-- Versión de PHP: 7.2.13-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel18`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'registrado', NULL, NULL),
(2, 'socio', NULL, NULL),
(3, 'administrador', NULL, NULL),
(4, 'root', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 4, 'Pepe', 'pepe@gmail.com', NULL, '$2y$10$UjFlPiw.WxoOO8qG0M3dpOwe/nD13aXHMxNx0g90WwgGtIsO7.7RW', 'k0syxqwHgBZO4SQmQN5Je002HUiLFg2fAs78W0cFSpB0EaT6olf8idGqb1fq', NULL, NULL),
(2, 3, 'Ana', 'ana@gmail.com', NULL, '$2y$10$Nb6tU67y2AGAmiGeh2zgp.9.S5SMkZwSt9yIHGk9LHmAHXNSh8m1u', '0HopjkT2ia', NULL, NULL),
(3, 2, 'User', 'user@gmail.com', NULL, '$2y$10$./M9fsJWkB5rmPSuv0gl0OxyXiQKTA.X6I3.D1dphK/1c8M0KWEEC', '5fpPJesN0Q', NULL, NULL),
(4, 1, 'Eulalia Robel', 'chelsea47@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'uR3LYW66lH', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(5, 1, 'Karen Hahn', 'dolly.ruecker@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'x8ZB9jioEG', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(6, 1, 'Miss Christina Daugherty', 'lehner.allene@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'i4NFh9dgso', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(7, 1, 'Mrs. Wilma Ondricka II', 'flatley.brandi@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'jFr0BXe2S0', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(8, 1, 'Mr. Terry Beer V', 'wilhelmine.barrows@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'drPoOG1r25', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(9, 1, 'Jerrold Bednar', 'dbecker@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'RjGcIdr2lq', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(10, 1, 'Kari Hoppe', 'hcrist@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '4rml9UaPwp', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(11, 1, 'Mr. Lionel Bode', 'emmett.rippin@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '910G52IdNo', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(12, 1, 'Baby Morar', 'reichert.ford@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'aRXn5CDzNd', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(13, 1, 'Dr. Therese Mraz III', 'will69@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '8dH1KyofPf', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(14, 1, 'Jared Metz', 'paige93@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'qGzbrpErRY', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(15, 1, 'Dr. Oren Stroman V', 'otis75@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'HE5TZgBbDm', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(16, 1, 'Sonny Herman DVM', 'will.bernadine@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'SIhqOAL9qp', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(17, 1, 'Kathryne Zieme', 'ursula.rohan@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ykdUymDzBv', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(18, 1, 'Donald Brekke', 'zquitzon@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'P0kc4Cj6jY', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(19, 1, 'Prof. Christop Koepp V', 'stefanie96@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'eqkEvkAQqI', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(20, 1, 'Mr. Kim Howell', 'deshawn.dubuque@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'DiDoK5EUm7', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(21, 1, 'Isadore Rolfson', 'dterry@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '6aKtjZuq1l', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(22, 1, 'Kailee Zulauf', 'xavier62@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'z0ZtgwvZrI', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(23, 1, 'Bonita Rutherford PhD', 'ratke.toney@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'JXMQv0Hjl0', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(24, 1, 'Noemy Cartwright Jr.', 'ila.boyle@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'EUjOEFxT5c', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(25, 1, 'Monserrate Bergnaum', 'penelope.wunsch@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'x2KgB6TC62', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(26, 1, 'Dr. Will Goldner', 'ppfannerstill@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'qdqFq0RE7G', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(27, 1, 'Mr. Everett Wuckert', 'idell32@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'odglGyTpxr', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(28, 1, 'Dr. Otha Hackett', 'rvandervort@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'crYtjobF7p', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(29, 1, 'Mr. Deron Hickle', 'rodriguez.evelyn@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'FYruJea9P7', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(30, 1, 'Emely Mills', 'foster59@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Epjyauf4rn', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(31, 1, 'Prof. Carmella Jacobi II', 'nrice@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'OZys3oRAhT', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(32, 1, 'Prof. Brennan Goodwin I', 'cameron.moore@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'sPvk5vtFgT', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(33, 1, 'Cristian Hamill', 'ritchie.roslyn@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'bSZiuDScwK', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(34, 1, 'Birdie Labadie', 'beaulah83@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ZIrYOotYFH', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(35, 1, 'Damon Flatley', 'esmeralda62@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'KwmcvwNldZ', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(36, 1, 'Karina Kuhlman', 'vern.mosciski@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '1haqzZmy3W', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(37, 1, 'Leopold Rice', 'nader.simone@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'RIZa5rDymv', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(38, 1, 'Lorenzo Cassin', 'feeney.karlee@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'piomPRldTA', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(39, 1, 'Katheryn Shanahan', 'wolf.odessa@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ewltwBcMAG', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(40, 1, 'Toney Kunze', 'beer.sandrine@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'QLIGEB9N8A', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(41, 1, 'Mr. Lon Kirlin', 'dickens.tina@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'UcI2xirKGw', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(42, 1, 'Dangelo McCullough', 'paula52@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'hjJaDJMK9j', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(43, 1, 'Miss Kathryn Gottlieb DDS', 'olen50@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 't9IE6dPNQh', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(44, 1, 'Ms. Mabel Bechtelar II', 'lschmeler@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '76I9UUjhhx', '2019-01-23 15:58:37', '2019-01-23 15:58:37'),
(45, 1, 'Emerald King', 'keeling.genevieve@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'o9v4IVrvW2', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(46, 1, 'Winona Schuppe III', 'geoffrey.kiehn@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '5vrn6iqiE5', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(47, 1, 'Dr. Sebastian Bartoletti', 'willis.metz@example.com', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ZtzGQ6AQZl', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(48, 1, 'Imogene Hilpert', 'lfahey@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'DrpaBJHPi9', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(49, 1, 'Prof. Arturo Greenfelder', 'daniela.paucek@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kZdkv8yUGk', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(50, 1, 'Murphy Lindgren', 'clindgren@example.net', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ESn7QrpDfH', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(51, 1, 'Cade Bashirian', 'russel85@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'NBFsvm5FMQ', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(52, 1, 'Guillermo Beer II', 'ofeest@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'dCUzmIiVV5', '2019-01-23 15:58:38', '2019-01-23 15:58:38'),
(53, 1, 'Bernice Durgan', 'liza.boyer@example.org', '2019-01-23 15:58:37', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'V4mYXlBggq', '2019-01-23 15:58:38', '2019-01-23 15:58:38');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
