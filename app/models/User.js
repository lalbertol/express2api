const connection = require('../config/dbconnection.js')

const index = function (callback) {
  const sql = 'SELECT * FROM users'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
      // res.json(result)
    }
  })
}
const find = function (id, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = 'SELECT * FROM users WHERE id = ?'

  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
    }
  })
}

const destroy = function (id, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = 'DELETE FROM users WHERE id = ?'

  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
    }
  })
}

const create = function (role_id, name, email, password, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = `INSERT INTO users(role_id, name, email, password) VALUES (?,?,?,?)`

  connection.query(sql, [role_id, name, email, password], (err, result) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result)
    }
  })
}

const update = function (id, name, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = 'UPDATE users SET name = ? WHERE id=?'
  connection.query(sql, [name, id], (err, result) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result)
    }
  })
}

module.exports = {
  index,
  find,
  destroy,
  create,
  update
}
