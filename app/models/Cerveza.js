const connection = require('../config/dbconnection.js')

const index = function (callback) {
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
      // res.json(result)
    }
  })
}
const find = function (id, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = 'SELECT * FROM cervezas WHERE id = ?'

  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
    }
  })
}

const destroy = function (id, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = 'DELETE FROM cervezas WHERE id = ?'

  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
    }
  })
}

const create = function (
  name,
  description,
  alcohol,
  container,
  price,
  callback
) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = `INSERT INTO cervezas(name,description,alcohol,container,price) VALUES (?,?,?,?,?)`

  connection.query(
    sql,
    [name, description, alcohol, container, price],
    (err, result) => {
      if (err) {
        callback(500, err)
      } else {
        callback(200, result)
      }
    }
  )
}

const update = function (id, name, callback) {
  // return 'Lista de cervezas de controller'

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
  const sql = 'UPDATE cervezas SET name = ? WHERE id=?'
  connection.query(sql, [name, id], (err, result) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result)
    }
  })
}

module.exports = {
  index,
  find,
  destroy,
  create,
  update
}
