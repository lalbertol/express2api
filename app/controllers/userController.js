const connection = require('../config/dbconnection.js')
const User = require('../models/User.js')

const index = function (req, res) {
  const users = User.index(function (status, data, fields) {
    res.status(status).json(data)
  })
}
const show = (req, res) => {
  const id = req.params.id
  const users = User.find(id, function (status, data, fields) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡A ver la User! ${req.params.id}` })
}

const destroy = (req, res) => {
  const id = req.params.id
  const users = User.destroy(id, function (status, data, fields) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡A tirar la User! ${req.params.id}` })
}
const store = (req, res) => {
  // return 'Lista de users de controller'
  const name = req.body.name
  const role_id = req.body.role_id
  const email = req.body.email
  const password = req.body.password

  const users = User.create(role_id, name, email, password, function (
    status,
    data
  ) {
    res.status(status).json(data)
  })

  // const sql = `SELECT * FROM users WHERE id = ${req.params.id}`
}
const update = (req, res) => {
  const id = req.params.id

  const users = User.update(id, req.body.name, function (status, data, fields) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡A actualizar la cerveza! ${req.body.nombre}` })
}
module.exports = {
  index,
  show,
  destroy,
  store,
  update
}
