const connection = require('../config/dbconnection.js')
const Cerveza = require('../models/Cerveza.js')

const index = function (req, res) {
  const cervezas = Cerveza.index(function (status, data, fields) {
    res.status(status).json(data)
  })
}
const show = (req, res) => {
  const id = req.params.id
  const cervezas = Cerveza.find(id, function (status, data, fields) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡A ver la cerveza! ${req.params.id}` })
}

const destroy = (req, res) => {
  const id = req.params.id
  const cervezas = Cerveza.destroy(id, function (status, data, fields) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡A tirar la cerveza! ${req.params.id}` })
}
const store = (req, res) => {
  // return 'Lista de cervezas de controller'
  const name = req.body.name
  const description = req.body.description
  const alcohol = req.body.alcohol
  const container = req.body.container
  const price = req.body.price

  const cervezas = Cerveza.create(
    name,
    description,
    alcohol,
    container,
    price,
    function (status, data) {
      res.status(status).json(data)
    }
  )

  // const sql = `SELECT * FROM cervezas WHERE id = ${req.params.id}`
}
const update = (req, res) => {
  const id = req.params.id

  const cervezas = Cerveza.update(id, req.body.name, function (
    status,
    data,
    fields
  ) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡A actualizar la cerveza! ${req.body.nombre}` })
}
module.exports = {
  index,
  show,
  destroy,
  store,
  update
}
