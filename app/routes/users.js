const express = require('express') // llamamos a Express
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const userController = require('../controllers/userController.js')
// establecemos nuestra primera ruta, mediante get.
// router.get('/', (req, res) => {
//   res.json({ mensaje: '¡Bienvenido a nuestra API!' })
// })

router.post('/', (req, res) => {
  userController.store(req, res)
  // res.json({ mensaje: `Cerveza creada : ${req.body.nombre}` })
})

router.delete('/:id', (req, res) => {
  userController.destroy(req, res)
  // res.json({ mensaje: 'Cerveza borrada' })
})
router.put('/:id', (req, res) => {
  userController.update(req, res)
  // res.json({ mensaje: 'Cerveza actualizada' })
})
router.get('/', (req, res) => {
  //  let page = req.query.page || 1;
  //  //let page = req.query.page ? req.query.page : 1;

  // res.json({ mensaje: `Lista de cervezas, página ${page}` })
  userController.index(req, res)
})

router.get('/:id', (req, res) => {
  userController.show(req, res)
  // res.json({ mensaje: `¡A beberte la cerveza! ${req.params.id}` })
})

module.exports = router
