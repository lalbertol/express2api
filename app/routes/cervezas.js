const express = require('express') // llamamos a Express
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const cervezaController = require('../controllers/cervezaController.js')
// establecemos nuestra primera ruta, mediante get.
// router.get('/', (req, res) => {
//   res.json({ mensaje: '¡Bienvenido a nuestra API!' })
// })

router.post('/', (req, res) => {
  cervezaController.store(req, res)
  // res.json({ mensaje: `Cerveza creada : ${req.body.nombre}` })
})

router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
  // res.json({ mensaje: 'Cerveza borrada' })
})
router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
  // res.json({ mensaje: 'Cerveza actualizada' })
})
router.get('/', (req, res) => {
  //  let page = req.query.page || 1;
  //  //let page = req.query.page ? req.query.page : 1;

  // res.json({ mensaje: `Lista de cervezas, página ${page}` })
  cervezaController.index(req, res)
})

router.get('/:id', (req, res) => {
  cervezaController.show(req, res)
  // res.json({ mensaje: `¡A beberte la cerveza! ${req.params.id}` })
})

module.exports = router
